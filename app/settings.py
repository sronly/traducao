# coding: utf-8

"""
Django settings for app project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import socket, os, sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


IP = socket.gethostbyname(socket.gethostname()).split('.')
PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
OS_SYSTEM = os.name
SITE_URL = 'http://estindcr13:8000'

ADMINS = (
    ('Jose Luis da Cruz Junior', 'jose.cruz@ourofino.com'),
)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_u^6u1!6!uk=v(t=iy%9h5a8)45a2))n%cbtj9j_4*bd%$(4^w'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

print os.path.join(PROJECT_PATH, 'templates')

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
)

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    # TRANSLATION
    'modeltranslation',

    # SYSTEM
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    #3RD PARTY
    'captcha',
    'ckeditor',
    'endless_pagination',
    'localflavor',
    'pagination',
    'sorl.thumbnail',

    #PROJECT
    'appviews',
    'conteudo',
    'util',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True
USE_L10N = True

ugettext = lambda s: s
LANGUAGES = (
    ('pt-br', ugettext(u'Português')),
    ('en', ugettext(u'Inglês')),
    ('es', ugettext(u'Espanhol')),
)


USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(PROJECT_PATH, 'static')

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'static'),
)

# CUSTOM SETTINGS
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')
MEDIA_URL = '/media/'

# CKUPLOADS CONFIGS
CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'uploads/ckuploads')
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Full',
        'height': 450,
        'width': 800,
        'toolbarCanCollapse': False,
    },
    'basic':{
        'toolbar': 'Basic',
        'height': 450,
        'width': 800,
        'toolbarCanCollapse': False,
    },
    'custom':{
        'toolbar': [
            [
                'Paste','PasteText','PasteFromWord','Undo', 'Redo',
                '-', 'Bold', 'Italic', 'Underline','Subscript','Superscript','RemoveFormat',
                '-', 'Link', 'Unlink', 'Anchor',
                '-', 'Format','NumberedList','-','BulletedList','Outdent','Indent',
                '-', 'Table','HorizontalRule',
                '-', 'Maximize',
            ],
        ],
        'height': 450,
        'width': '100%',
        'toolbarCanCollapse': False,

    },
    'custom_upload':{
        'toolbar': [
            [
                'Paste','PasteText','PasteFromWord','Undo', 'Redo',
                '-', 'Bold', 'Italic', 'Underline','Subscript','Superscript','RemoveFormat',
                '-', 'Link', 'Unlink', 'Anchor',
                '-', 'Image',
                '-', 'Format','NumberedList','-','BulletedList','Outdent','Indent',
                '-', 'Table','HorizontalRule',
                '-', 'Maximize',
            ],
        ],
        'height': 450,
        'width': '100%',
        'toolbarCanCollapse': False,

    }
}


# DJANGO SIMPLE CAPTCHA
CAPTCHA_LENGTH = 8
CAPTCHA_FONT_SIZE = 24
CAPTCHA_FOREGROUND_COLOR = "#2D89D4"
#CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'
#CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.random_char_challenge' #DEFAULT
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.word_challenge'
CAPTCHA_WORDS_DICTIONARY = os.path.join(PROJECT_PATH, 'captcha.txt')


# EMAIL AUTENTICACAO GMAIL
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'portal@midiadigitalourofino.com'
EMAIL_HOST_PASSWORD = 'kSXJHO0jwCBMPluP'
EMAIL_PORT = 587



try:
    from local_settings import *
except:
    pass