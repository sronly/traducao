# coding: utf-8
from django.conf.urls import patterns, include, url
from . import views as views

urlpatterns = patterns('',

	############################################################################
	# INDEX
	url(r'^$', views.IndexView.as_view(), name='index'),
	############################################################################


)
