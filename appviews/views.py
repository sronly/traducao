# Create your views here.
# coding: utf-8

from django.conf import settings
from django.shortcuts import render_to_response, get_object_or_404
from django.template import Context, loader, RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import EmailMultiAlternatives

from django.views.generic import TemplateView, View, ListView, RedirectView
from django.views.generic.edit import FormView, CreateView, UpdateView

import re
from django.db.models import F, Q
import urllib2
from datetime import datetime, date

from util import util as U
from conteudo.models import Conteudo



#############################################################
# INDEX
#############################################################
class IndexView(TemplateView):
	template_name = 'index.html'

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		context.update({
			'index': True,
			'conteudo': get_object_or_404(Conteudo, id=1),
			'settings':settings
		})
		return context
