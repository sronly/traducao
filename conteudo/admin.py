# Register your models here.

from django import forms
from django.contrib import admin
from .models import *
from modeltranslation.admin import TranslationAdmin

class ConteudoAdmin(TranslationAdmin):
	search_fields = ('titulo', 'texto',)
	list_display = ('titulo', 'texto')
	save_on_top = True

admin.site.register(Conteudo, ConteudoAdmin)