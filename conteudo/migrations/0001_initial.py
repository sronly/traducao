# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Conteudo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=255, verbose_name='T\xedtulo')),
                ('titulo_en', models.CharField(max_length=255, null=True, verbose_name='T\xedtulo')),
                ('titulo_es', models.CharField(max_length=255, null=True, verbose_name='T\xedtulo')),
                ('texto', models.TextField()),
                ('texto_en', models.TextField(null=True)),
                ('texto_es', models.TextField(null=True)),
            ],
            options={
                'ordering': ('titulo',),
                'verbose_name': 'Conteudo',
                'verbose_name_plural': 'Conteudos',
            },
            bases=(models.Model,),
        ),
    ]
