# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conteudo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='conteudo',
            name='texto_pt_br',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conteudo',
            name='titulo_pt_br',
            field=models.CharField(max_length=255, null=True, verbose_name='T\xedtulo'),
            preserve_default=True,
        ),
    ]
