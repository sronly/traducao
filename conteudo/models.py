# coding: utf-8
from django.db import models

# Create your models here.

class Conteudo(models.Model):
	"""(Conteudo description)"""
	titulo = models.CharField(u'Título', max_length=255)
	texto = models.TextField()


	class Meta:
		verbose_name, verbose_name_plural = u"Conteudo" , u"Conteudos"
		ordering = ('titulo',)

	def __unicode__(self):
		return u"%s" % self.titulo