function translate(){
	// alteracao de linguagem
	var i18nURL = '/i18n/setlang/';
	var csrfToken = $('input[name="csrfmiddlewaretoken"]').val();
	var langButtons = $('.setlang-button');
	var lang;
	var next = window.location.href;

	langButtons.bind('click', function(event) {
		event.preventDefault();
		lang = $(this).data('lang');
		$.post(i18nURL, {
			'csrfmiddlewaretoken' : csrfToken,
			'language' : lang
		}, function() {
			window.location.href = next;
		});
	});
}
