class EmailExistente(Exception):
	pass

class CPFExistente(Exception):
	pass

class CNPJExistente(Exception):
	pass

class ErroCadastro(Exception):
	pass

class ErroContato(Exception):
	pass
	