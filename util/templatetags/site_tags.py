# -*- coding: utf-8 -*-
import locale
import datetime

#
#from sorl.thumbnail.main import DjangoThumbnail

from django import template
from django.conf import settings
from django.template import Library, Node
from django.template import defaultfilters
from django.template import defaulttags


from decimal import Decimal


register = Library()

@register.filter
def zeroFill(valor,qtd):
	return str(valor).rjust(qtd, '0')

@register.filter
def image_url_regex(html):
	import re
	from sorl.thumbnail import get_thumbnail
	from PIL import Image
	from django.conf import settings

	html = str(html)
	try:
		m = re.search('uploads/[a-z0-9]*/[0-9]{4}/[A-Za-z0-9-_]*.[a-zA-Z]{3}', html)
		jpgfile = Image.open("%s/%s" % ( settings.MEDIA_ROOT,  m.group(0) ) )
		im = get_thumbnail(jpgfile.filename, '100x100', crop='center', quality=99)
		return '<img src="%s" />' % im.url
	except AttributeError:
		return "%s" % html



# ###################################################################
# PRODUTOS TAGS / FILTERS
# ###################################################################

@register.simple_tag
def site_url():
	return settings.SITE_URL