# coding: utf-8

#################################################
# LOCALFLAVOR
#################################################
#from django.contrib.localflavor.br.br_states import STATE_CHOICES
from localflavor.br.br_states import STATE_CHOICES


#################################################
# FUNCTIONS
#################################################

def extract_a_from_html(html):
	import re
	return re.findall(r'href=[\'"]?([^\'" >]+)', html)