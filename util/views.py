# Create your views here.
# -*- coding: utf-8 -*-

from django.conf import settings
from django.shortcuts import render_to_response, get_object_or_404
from django.template import Context, loader, RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import EmailMultiAlternatives


def HttpResponseDownload(texto, name=None, debug=False):
	if not debug:
		response = HttpResponse(u"%s" % texto)
		response['Content-Type'] = 'application/force-download; charset=utf-8'
		response['Pragma'] = 'public'
		response['Expires'] = '0'
		response['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0'
		response['Content-Disposition'] = 'attachment; filename=%s' % name
		response['Content-Transfer-Encoding'] = 'binary'
		response['Content-Length'] = unicode(len(texto)+1024)

		return response
	else:
		return HttpResponse("<pre>"+texto+"</pre>")